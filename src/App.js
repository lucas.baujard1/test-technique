import { useState } from "react";
import { KEYBOARD_KEYS, ENUM_KEYBOARD } from "./constants";
import CheckCircle from "./check_circle.svg";
import Backspace from "./backspace.svg";
import Capslock from "./capslock.svg";
import Return from "./return.svg";
import Space from "./space.svg";
import "./App.css";

const getIcon = (iconName) => {
  switch (iconName) {
    case ENUM_KEYBOARD.BACKSPACE:
      return Backspace;
    case ENUM_KEYBOARD.CHECK_CIRCLE:
      return CheckCircle;
    case ENUM_KEYBOARD.RETURN:
      return Return;
    case ENUM_KEYBOARD.CAPSLOCK:
      return Capslock;
    case ENUM_KEYBOARD.SPACE:
      return Space;
    default:
      return null;
  }
};

const App = () => {
  const [isFocus, setIsFocus] = useState(false);
  const [input, setInput] = useState("");
  const [isMaj, setIsMaj] = useState(false);

  const handleChange = (key) => {
    switch (key) {
      case ENUM_KEYBOARD.SPACE:
        return setInput(input + " ");
      case ENUM_KEYBOARD.BACKSPACE:
        return setInput(input?.slice(0, input?.length - 1));
      case ENUM_KEYBOARD.RETURN:
        return setInput(input + "\n");
      case ENUM_KEYBOARD.CAPSLOCK:
        return setIsMaj(!isMaj);
      case ENUM_KEYBOARD.CHECK_CIRCLE:
        return input;
      default:
        return setInput(input + (isMaj ? key.toUpperCase() : key));
    }
  };

  const handleFocus = () => {
    setIsFocus(true);
  };

  return (
    <div className="container">
      <textarea
        key="textarea"
        className="textarea"
        value={input}
        onFocus={handleFocus}
        readOnly
      />
      {isFocus && (
        <div className="keyboard">
          {KEYBOARD_KEYS.map((keyLine, i) => (
            <ul key={`line-${i + 1}`} className="line">
              {keyLine?.map((k) => (
                <li
                  key={k}
                  className={isMaj ? "key uppercase" : "key"}
                  onClick={() => handleChange(k)}
                >
                  {k.length === 1 ? k : <img src={getIcon(k)} alt={k} />}
                </li>
              ))}
            </ul>
          ))}
        </div>
      )}
    </div>
  );
};

export default App;
