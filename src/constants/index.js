export const KEYBOARD_KEYS = [
  ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "backspace"],
  ["q", "w", "e", "r", "t", "y", "u", "i", "o", "p"],
  ["capslock", "a", "s", "d", "f", "g", "h", "j", "k", "l", "return"],
  ["checkCircle", "z", "x", "c", "v", "b", "n", "m", ",", ".", "?"],
  ["space"],
];

export const ENUM_KEYBOARD = {
  BACKSPACE: "backspace",
  CAPSLOCK: "capslock",
  CHECK_CIRCLE: "checkCircle",
  RETURN: "return",
  SPACE: "space",
};
